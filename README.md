# Lab4-Star-Wars-React-app
App project for Lab 4 of Darksaber course

## Updates made to app
- Added Vader.svg file and added image to App.js
- Added a second lightsaber.png image, imported it, and added to App.js App component
- Updated App component header and paragraph text
- Added a date that updates each day automatically with JS Date object
- Added alt text to each image

## How to run the app
- Clone the app onto your desktop
- On the terminal run `cd Lab4-Star-Wars-React-app` to move into the directory
- Once in the directory, run `npm start` in the terminal